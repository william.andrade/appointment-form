const path = require('path');

const PATHS = {
  src: path.join(__dirname, 'src'),
  dist: path.join(__dirname, 'dist'),
  css: path.join(__dirname, 'dist/css')
};

module.exports = {
  entry: "./src/index.js",
  output: {
    path: PATHS.dist,
    filename: "bundle.js",
  },
  devServer: {
    hot: true,
    contentBase: './dist'
  },
  devtool: 'inline-source-map',
  module: {
    loaders: [
      { //loaders -- Specifies how each file should be processed before combining into bundle
      test: /\.js$/,
      exclude: /node_modules/,
      loader: "babel-loader", //convert ES6+JSX JavaScript into ES5 (which moder browsers understand)
      include: path.join(__dirname, 'src'),
      options: {
        presets: ["es2015", "react"]
      }
    },
      {
        test: /\.(jpg|png|svg)$/,
        loader: "file-loader",
        options: {
          name: 'assets/images/[name].[ext]'
        }
      },
      {
        test:  /\.css$/,
        loaders: ['style-loader', 'css-loader'],
      }
    ]
  },
  resolve: { //Where webpack should look for referenced by import or require(). Makes it so that I can import npm packages in code
    modules: [
      path.join(__dirname, 'node_modules')
    ]
  }
};
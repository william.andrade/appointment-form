import {
  APPOINTMENT_SUBMIT,
  DIALOG_CANCEL,
  NAME_UPDATE,
  PHONE_UPDATE,
  SLOT_SELECTED,
  EVENT_SELECTED,
  EVENT_DELETED
} from './appointment-actions'

//define the initial state
const defaultState = () => {
  return {
    events: [],
    open: false,
    eventObj: {
      name: '',
      phoneNumber: '',
      title: '',
      start: undefined,
      end: undefined,
      submitted: false
    }
  }
};

const AppointmentForm = (state = defaultState(), action = {}) => {
  switch (action.type) {
    case NAME_UPDATE:
      return {
        ...state,
        eventObj: {
          ...state.eventObj,
          name: action.payload.name
        }
      };
    case PHONE_UPDATE:
      return {
        ...state,
        eventObj: {
          ...state.eventObj,
          phoneNumber: action.payload.phoneNumber
        }
      };
    case SLOT_SELECTED:
      return {
        ...state,
        open: true,
        eventObj: {
          ...state.eventObj,
          start: action.payload.start,
          end: action.payload.end
        }
      };
    case EVENT_DELETED:
      let events = [].concat(state.events);
      let index = events.findIndex(eventObj => eventObj.start === action.payload.start);
      events.splice(index, 1);
      return {
        ...state,
        open: false,
        eventObj: {},
        events: events
      };

    case EVENT_SELECTED:
      return {
        ...state,
        open: true,
        eventObj: {
          ...action.payload
        }
      };
    case APPOINTMENT_SUBMIT:
      let submittedEvent = {
        ...state.eventObj,
        name: action.payload.name,
        phoneNumber: action.payload.phoneNumber,
        title: `Meeting with: ${action.payload.name} \u00A0\u00A0  -- \u00A0\u00A0 Contact information:  ${action.payload.phoneNumber}`,
        submitted: true
      };

      let tempEvents = [].concat(state.events);

      /**
       * Logic for handling updating an event (aka replacing an object in an array if start time property matches)
       * 1. We only need to worry about the possibility of updating an event if any exist
       * 2. We find the index of the object if the start times match with the submitted (aka updated) object
       * 3. If it matches, we replace the submitted (aka updated) object. This helps us avoid any overlapping events
       * 4. If it DOES NOT match, we push the submitted event into our events array
       */
      if (tempEvents.length) {
        let index = tempEvents.findIndex(eventObj => eventObj.start === submittedEvent.start);
        if (tempEvents[index]) {
          tempEvents[index] = submittedEvent;
        } else {
          tempEvents.push(submittedEvent);
        }
      } else {
        tempEvents.push(submittedEvent);
      }
      return {
        ...state,
        open: false,
        events: tempEvents,
        eventObj: {}
      };
    case DIALOG_CANCEL:
      return {
        ...state,
        open: false,
        eventObj: {},
        events: [...state.events]
      };
    default:
      return state;
  }

};

export default AppointmentForm;
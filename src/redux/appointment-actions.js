/**
 * Redux Actions for Appointment Actions
 */
const APPOINTMENT_SUBMIT = 'APPOINTMENT_SUBMIT';
const DIALOG_CANCEL = 'DIALOG_CANCEL';
const NAME_UPDATE = 'NAME_UPDATE';
const PHONE_UPDATE = 'PHONE_UPDATE';
const SLOT_SELECTED = 'SLOT_SELECTED';
const EVENT_SELECTED = 'EVENT_SELECTED';
const EVENT_DELETED = 'EVENT_DELETED';


const nameUpdate = payload => ({
  type: NAME_UPDATE,
  payload
});

const phoneUpdate = payload => ({
  type: PHONE_UPDATE,
  payload
});

const dialogClose = payload => ({
  type: DIALOG_CANCEL,
  payload
});

const appointmentSubmit = payload => ({
  type: APPOINTMENT_SUBMIT,
  payload
});

const slotSelected = payload => ({
  type: SLOT_SELECTED,
  payload
});

const eventSelected = payload => ({
  type: EVENT_SELECTED,
  payload
});

const eventDeleted = payload => ({
  type: EVENT_DELETED,
  payload
});

export {

  DIALOG_CANCEL,
  dialogClose,

  APPOINTMENT_SUBMIT,
  appointmentSubmit,

  SLOT_SELECTED,
  slotSelected,

  EVENT_SELECTED,
  eventSelected,

  NAME_UPDATE,
  nameUpdate,

  PHONE_UPDATE,
  phoneUpdate,

  EVENT_DELETED,
  eventDeleted

};



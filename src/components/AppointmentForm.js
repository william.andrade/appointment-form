import React, {Component} from 'react';
import BigCalendar from 'react-big-calendar';
import {connect} from 'react-redux';
import {slotSelected, eventSelected} from '../redux/appointment-actions';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import moment from 'moment';
import AppointmentDialog from './AppointmentDialog';
BigCalendar.momentLocalizer(moment);


const times = {
  minTime: '09:00',
  maxTime: '18:00'
};

const currentDate = moment().format('D MMM YYYY');


class AppointmentForm extends Component {
  constructor(props) {
    super(props);

    this.onSlotSelected = this.onSlotSelected.bind(this);
    this.onEventSelected = this.onEventSelected.bind(this);
    this.eventStyleGetter = this.eventStyleGetter.bind(this);


  }

  eventStyleGetter() {
    let backgroundColor = '#' + 'cc0000';
    let style = {
      backgroundColor: backgroundColor,
      borderRadius: '0px',
      opacity: 0.8,
      color: '#FFFFFF',
      border: '0px',
      display: 'block'
    };
    return {
      style: style
    };
  }

  onEventSelected(selectedEvent) {
    const {onEventSelect} = this.props;

    if(onEventSelect) {
      onEventSelect(selectedEvent);
    }

  }

  onSlotSelected(slotInfo) {
    const {onSlotSelect} = this.props;

    if (slotInfo.action === 'click') {
      const payload = {
        start: new Date(`${slotInfo.start}`),
        end: new Date(`${slotInfo.end}`)
      };

      if (onSlotSelect) {
        onSlotSelect(payload);
      }
    } else {
      return;
    }
  }

  render() {

    return (
      <form id="appointment-form">
        <h2 id="appointment-header">{'Appointment Form'}</h2>

        <AppointmentDialog/>

        <BigCalendar
          selectable={'ignoreEvents'}
          timeslots={1}
          toolbar={false}
          style={{height: '500px'}}
          defaultView='day'
          views={['day']}
          events={this.props.events}
          step={60}
          min={new Date(`${currentDate}, ${times.minTime}`)}
          max={new Date(`${currentDate}, ${times.maxTime}`)}
          onSelectSlot={(slotInfo) => this.onSlotSelected(slotInfo)}
          onSelectEvent={(event) => this.onEventSelected(event)}
          eventPropGetter={this.eventStyleGetter}
        />
      </form>
    );
  }
}

AppointmentForm = connect(
  (state, ownProps) => {
    const events = state.events ? state.events : [];

    return {
      events
    };
  },
  dispatch => ({
    onSlotSelect: (obj) => dispatch(slotSelected(obj)),
    onEventSelect: (obj) => dispatch(eventSelected(obj))
  })
)(AppointmentForm);

export default AppointmentForm;

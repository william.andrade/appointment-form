import React, { Component } from "react";
import AppointmentForm from './AppointmentForm';
import Paper from 'material-ui/Paper';

export default class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {

    return (
      <Paper id="appointment-container" zdepth={2}>
        <AppointmentForm/>
      </Paper>
    );
  }
}

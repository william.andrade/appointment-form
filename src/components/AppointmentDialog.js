import React, {Component} from 'react';
import {connect} from 'react-redux';
import {dialogClose, nameUpdate, phoneUpdate, appointmentSubmit, eventDeleted} from '../redux/appointment-actions';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';


class AppointmentDialog extends Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
    this.onCancel = this.onCancel.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onNameChange = this.onNameChange.bind(this);
    this.onPhoneChange = this.onPhoneChange.bind(this);

  }

  onSubmit() {
    const {onSubmit, eventObj} = this.props;

    const payload = {
      name: eventObj.name,
      phoneNumber: eventObj.phoneNumber
    };

    if (onSubmit) {
      onSubmit(payload)
    }

  }

  onCancel() {
    const {onCancel} = this.props;

    const payload = {
      open: true
    };

    if (onCancel) {
      onCancel(payload);
    }
  }

  onDelete(eventObj){
    const {onDelete} = this.props;

    if(onDelete) {
      onDelete(eventObj);
    }
  }

  onNameChange(event) {
    const {onNameUpdate} = this.props;

    const payload = {
      name: event.target.value
    };

    if (onNameUpdate) {
      onNameUpdate(payload);
    }
  }

  onPhoneChange(event) {
    const {onPhoneUpdate} = this.props;

    const payload = {
      phoneNumber: event.target.value
    };

    if (onPhoneUpdate) {
      onPhoneUpdate(payload);
    }
  }


  render() {

    const {open, eventObj} = this.props;

    const disabled = !eventObj.name || !eventObj.phoneNumber ? true : false;

    const dialogActions = [
      <FlatButton
        label="Delete"
        disabled={!eventObj.submitted}
        onClick={()=>this.onDelete(eventObj)}
        labelStyle={{color: !eventObj.submitted ? '' : '#DF0909'}}
      />,
      <FlatButton
        label="Cancel"
        onClick={this.onCancel}
      />,
      <FlatButton
        label="Submit"
        onClick={this.onSubmit}
        disabled={disabled}
        primary={true}
      />
    ];

    return (
      <div>
        <Dialog
          title="Appointment Details"
          actions={dialogActions}
          bodyStyle={{display: 'flex', justifyContent: 'space-around'}}
          modal={true}
          open={open}>

          <TextField
            floatingLabelText={'Name'}
            floatingLabelFocusStyle={{color: '#337ab7'}}
            underlineFocusStyle={{borderColor: '#337ab7'}}
            value={eventObj.name ? eventObj.name : ''}
            onChange={this.onNameChange}
          />
          <TextField
            floatingLabelText={'Phone Number'}
            floatingLabelFocusStyle={{color: '#337ab7'}}
            underlineFocusStyle={{borderColor: '#337ab7'}}
            value={eventObj.phoneNumber ? eventObj.phoneNumber : ''}
            onChange={this.onPhoneChange}
          />
        </Dialog>
      </div>
    );
  }
}

AppointmentDialog = connect(
  (state, ownProps) => {

    const eventObj = state.eventObj;
    const open = state.open;

    return {
      open,
      eventObj
    };
  },
  dispatch => ({
    onCancel: (obj) => dispatch(dialogClose(obj)),
    onNameUpdate: (obj) => dispatch(nameUpdate(obj)),
    onPhoneUpdate: (obj) => dispatch(phoneUpdate(obj)),
    onSubmit: (obj) => dispatch(appointmentSubmit(obj)),
    onDelete: (obj) => dispatch(eventDeleted(obj))
  })
)(AppointmentDialog);


export default AppointmentDialog;

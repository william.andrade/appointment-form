import 'react-hot-loader/patch';
import '../dist/css/style.css';
import React from 'react';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import {render} from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppointmentForm from './redux/appointment-reducers';
import App from './components/App';

let store = createStore(AppointmentForm);
const containerEl = document.getElementById('app');

render(
  <MuiThemeProvider>
    <Provider store={store}>
      <App/>
    </Provider>
  </MuiThemeProvider>,
  containerEl
);
# React Appointment Form #

A simple react form to simulate making appointments. It's built using [React] (https://reactjs.org/) and [Redux] (http://redux.js.org/). Additionally, I used [react-big-calendar] (https://github.com/intljusticemission/react-big-calendar) and [Material-UI] (http://www.material-ui.com/#/) as the primary UI components. It's a simple application, but the user should be able to create, update, and delete events they enter on the calendar.

Notes:
---
* __Events__: 
    * __Create__ - Click on a time slot on the calendar to create an event. A dialog should pop up for user input as soon as you do
    * __Edit__ - Click on an exiting event to display its information. You should be able to change anything in the two text fields and submit updates

* __[Submit] Button__: 
    * Only becomes enabled if you have a Name and Phone number value entered in the dialog
    * If you are editing an already created event, the same rules apply. You can edit the values and press the [Submit] button to update the event

* __[Cancel] Button__: 
	* Is enabled at all times
	* If you click on a time slot, then decide you dont want to create the appointment, press the [Cancel] button to exit the dialog
	* If you click on an already created event to edit, then change your mind, press the [Cancel] button to exit the dialog

* __[Delete] Button__:
	* Is only enabled for a created event that's on the calendar (disabled otherwise)
	* Click on the event to open the dialog, then press the [Delete] button to remove the event from the calendar


Setup/Install dependencies 
---

```
npm install
```

Usage
---

Start the dev server with this commmand:

```
npm start
```

Start the production server with this commmand:

```
npm run server
```

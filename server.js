import express from 'express';
import morgan from 'morgan';

const app = express();
const port = process.env.PORT || 8080;
const env = process.env.NODE_ENV || 'production';

app.use(morgan('dev'));
app.use(express.static(__dirname + '/dist'));

app.listen(port, err => {
  if(err) {
    return console.error(err);
  }
  console.info(`Server running on http://localhost:${port} [${env}]`);
});
